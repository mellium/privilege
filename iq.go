package "mellium.im/community/privilege"

import (
	"context"
	"encoding/xml"
	"fmt"
	"mellium.im/xmlstream"
	"mellium.im/xmpp"
	"mellium.im/xmpp/forward"
	"mellium.im/xmpp/stanza"
)

func WrapIQ(inner_iq xml.TokenReader, top_iq stanza.IQ) xml.TokenReader {
	xmlName := xml.Name{NSPrivilege,"privileged_iq"}
	privilegedWrap := xml.StartElement{Name: xmlName}
	return top_iq.Wrap(xmlstream.Wrap(inner_iq,privilegedWrap))
}

func UnwrapIQ(r xml.TokenReader) (xml.TokenReader, error) {
	// unwrap top-level IQ
	token, err := r.Token()
	if err != nil {
		return nil, err
	}
	se, ok := token.(xml.StartElement)
	if !ok {
		return nil, fmt.Errorf("expected a startElement, found %T", token)
	}
	if se.Name.Local != "iq" {
		return nil, fmt.Errorf("not an iq")
	}
	// unwrap privileged element
	stream, token, err := xmlstream.Unwrap(r)
	if err != nil {
		return nil, err
	}
	se, ok = token.(xml.StartElement)
	if !ok {
		return nil, fmt.Errorf("expected a startElement, found %T", token)
	}
	if se.Name.Local != "privilege" || se.Name.Space != NSPrivilege {
		return nil, fmt.Errorf("unexpected name for the privileged element: %+v", se.Name)
	}	
	// unwrap forwarded
	return forward.Unwrap(nil, stream) 
}

func SendIQ(inner_iq xml.TokenReader, top_iq stanza.IQ, ctx context.Context, s *xmpp.Session) (xml.TokenReader, error) {
	wrapped_iq := WrapIQ(inner_iq, top_iq)
	resp, err := s.SendIQ(ctx,wrapped_iq)
	if err != nil {
		return nil, err
	}
	return UnwrapIQ(resp)	
}
