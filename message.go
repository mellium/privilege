package "mellium.im/community/privilege"

import (
	"context"
	"encoding/xml"
	"mellium.im/xmlstream"
	"mellium.im/xmpp"
	"mellium.im/xmpp/forward"
	"mellium.im/xmpp/stanza"
)

func WrapMessage(inner_msg xml.TokenReader, top_msg stanza.Message) xml.TokenReader {
	privilegeName := xml.Name{NSPrivilege,"privilege"}
	forwardName := xml.Name{forward.NS,"forwarded"}
	forwardWrap := xml.StartElement{Name: forwardName}
	privilegeWrap := xml.StartElement{Name: privilegeName}
	return top_msg.Wrap(xmlstream.Wrap(xmlstream.Wrap(inner_msg,forwardWrap),privilegeWrap))
}

func SendMessage(inner_msg xml.TokenReader, top_msg stanza.Message, ctx context.Context, s *xmpp.Session) error {
	wrapped_msg := WrapMessage(inner_msg, top_msg)
	return s.Send(ctx,wrapped_msg)
}
