package "mellium.im/community/privilege"

import "mellium.im/xmpp/stanza"

const (
	NSPrivilege = "urn:xmpp:privilege:2"
)

type PrivilegeAdvertisement struct {
	stanza.Message
	Privilege struct {
		Perm  []struct {
			Access string `xml:"access,attr"`
			Type   string `xml:"type,attr,omitempty"`
			Push   string `xml:"push,attr,omitempty"`
			Namespace []struct {
				Ns string `xml:"ns,attr"`
				Type string `xml:"type,attr"`
			} `xml:"namespace,omitempty"`
		} `xml:"perm"`
	} `xml:"privilege"`
}
